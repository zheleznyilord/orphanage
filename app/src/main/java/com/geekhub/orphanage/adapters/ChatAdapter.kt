package com.geekhub.orphanage.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.geekhub.orphanage.db.entities.Message
import com.geekhub.orphanage.R
import kotlinx.android.synthetic.main.message_row.view.*

class ChatAdapter(private val messages: MutableList<Message>, val context: Context) :
    RecyclerView.Adapter<ChatAdapter.MsgViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MsgViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.message_row, parent, false)
        return MsgViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        return messages.size
    }

    override fun onBindViewHolder(holder: MsgViewHolder, position: Int) {
        holder.msgUserName.text = messages[position].userName
        holder.msgText.text = messages[position].messageText
    }

    fun setChat(msgs: MutableList<Message>?) {
        if (msgs != null) {
            messages.clear()
            messages.addAll(msgs)
            notifyDataSetChanged()
        }
    }
    inner class MsgViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val msgUserName = view.tvUserName!!
        val msgText = view.tvMsgText!!
    }

}
