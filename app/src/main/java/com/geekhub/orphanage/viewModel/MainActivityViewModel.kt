@file:Suppress("DEPRECATION")

package com.geekhub.orphanage.viewModel

import android.app.Application
import android.content.ContentValues.TAG
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.geekhub.orphanage.db.entities.Task
import com.geekhub.orphanage.db.MainDB
import com.geekhub.orphanage.db.TaskDao
import com.geekhub.orphanage.db.TaskRepository
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.coroutines.launch

@Suppress("DEPRECATION")
class MainActivityViewModel(
    application: Application,
    private val database: DatabaseReference = FirebaseDatabase.getInstance().reference,
    private val tasksDao: TaskDao = MainDB.getDB(application).taskDao(),
    val repository: TaskRepository = TaskRepository(tasksDao)
) : AndroidViewModel(application) {

    private var _tasks = MutableLiveData<MutableList<Task>>()
    val tasks: LiveData<MutableList<Task>> get() = _tasks

    val pref: SharedPreferences = application.getSharedPreferences("pref", Context.MODE_PRIVATE)

    private val connectivityManager =
        application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    private val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
    val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

    fun getTasksFromDB() {

        val taskListener = object : ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                Log.w(TAG, "loadTask:onCancelled", databaseError.toException())
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val taskList = mutableListOf<Task>()
                if (dataSnapshot.child("tasks").exists()) {
                    deleteAll()
                    for (snap in dataSnapshot.child("tasks").children) {
                        val newTask = Task(
                            snap.child("id").value.toString(),
                            snap.child("name").value.toString(),
                            snap.child("orphanage").value.toString()
                        )
                        taskList.add(newTask)
                        _tasks.postValue(taskList)
                        insert(newTask)
                    }
                } else {
                    _tasks.postValue(taskList)
                    deleteAll()
                }
            }
        }
        database.addValueEventListener(taskListener)
    }

    fun deleteTask(id: String) {
        database.child("tasks").child(id).setValue(null)
    }

    fun toMyList(id: String, name: String?, orphanage: String?) {
        val task = Task(id, name, orphanage)
        val user = FirebaseAuth.getInstance().currentUser
        deleteTask(id)
        database.child("myTasks").child(user?.displayName!!).child(id).setValue(task)
    }

    fun insert(task: Task) = viewModelScope.launch {
        repository.insert(task)
    }

    private fun deleteAll() = viewModelScope.launch {
        tasksDao.deleteAll()
    }

    private fun deleteFromRoom(task: Task) = viewModelScope.launch {
        tasksDao.deleteTask(task)
    }

}
