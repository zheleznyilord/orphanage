@file:Suppress("DEPRECATION")

package com.geekhub.orphanage

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.geekhub.orphanage.adapters.ChatAdapter
import com.geekhub.orphanage.viewModel.ChatViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.fragment_chat.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

private lateinit var adapter: ChatAdapter

class ChatFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_chat, container, false)
        val messageList = rootView.findViewById(R.id.messageList) as RecyclerView
        adapter = ChatAdapter(mutableListOf(), activity!!.application)
        messageList.layoutManager = LinearLayoutManager(activity)
        messageList.adapter = adapter

        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val viewModel = ChatViewModel(activity!!.application)

        val isConnected = viewModel.isConnected

        val pref = viewModel.pref
        val prefValue = pref.getBoolean("pref", false)

        CoroutineScope(Dispatchers.Default).launch {
            viewModel.getMsgsFromDB()
        }

        if (isConnected) {
            viewModel.msgs.observe(viewLifecycleOwner, Observer {
                adapter.setChat(it)
            })
        } else {
            viewModel.repository.allMessages.observe(viewLifecycleOwner, Observer {
                adapter.setChat(it)
            })
        }

        sendBtn.setOnClickListener {
            if (isConnected) {
                if (messageET.text.toString() != "") {
                    viewModel.writeNewMessage(
                        messageET.text.toString()
                    )
                    messageET.text.clear()
                } else {
                    Toast.makeText(
                        activity!!.applicationContext,
                        getString(
                            if (prefValue)
                                R.string.empty_message_meme
                            else
                                R.string.empty_message
                        ),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else {
                Toast.makeText(
                    activity,
                    getString(
                        if (prefValue)
                            R.string.no_network_meme
                        else
                            R.string.no_network
                    ),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
}