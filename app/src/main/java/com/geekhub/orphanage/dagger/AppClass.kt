package com.geekhub.orphanage.dagger

import android.app.Application
import com.geekhub.orphanage.dagger.components.AppComponent
import com.geekhub.orphanage.dagger.components.DaggerAppComponent
import com.geekhub.orphanage.dagger.modules.ApplicationModule

@Suppress("DEPRECATION")
open class AppClass : Application() {

    companion object {

        lateinit var component: AppComponent
            private set
    }

    override fun onCreate() {
        super.onCreate()
    }

    protected open fun buildDaggerComponent(): AppComponent {
        return DaggerAppComponent.builder().applicationModule(ApplicationModule(this)).build()
    }
}