package com.geekhub.orphanage.dagger.components

import android.app.Application
import com.geekhub.orphanage.dagger.AppClass
import com.geekhub.orphanage.dagger.modules.ApplicationModule
import com.geekhub.orphanage.ui.MainActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [ApplicationModule::class]
)
interface AppComponent {

    fun inject(application: Application)
}