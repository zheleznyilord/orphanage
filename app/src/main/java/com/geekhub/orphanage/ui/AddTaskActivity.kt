package com.geekhub.orphanage.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.geekhub.orphanage.R
import com.geekhub.orphanage.viewModel.AddTaskViewModel
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_add_task.*

class AddTaskActivity : AppCompatActivity() {

    private val viewModel = AddTaskViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_task)
        //TODO: Add location listener


        addTaskToDB.setOnClickListener {
            if ((this.taskET.text.toString() != "") && (this.orphanageET.text.toString() != "")) {
                viewModel.writeNewTask(
                    taskET?.text.toString(),
                    orphanageET?.text.toString()
                )
                finish()
            } else {
                Toast.makeText(this, "Task lines can not be empty", Toast.LENGTH_LONG).show()
            }
        }
    }

}
