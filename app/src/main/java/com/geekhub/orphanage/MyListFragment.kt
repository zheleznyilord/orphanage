@file:Suppress("DEPRECATION")

package com.geekhub.orphanage

import android.app.AlertDialog
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.geekhub.orphanage.adapters.MyListAdapter
import com.geekhub.orphanage.viewModel.MyListViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

private lateinit var adapter: MyListAdapter

class MyListFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_my_list, container, false)
        adapter = MyListAdapter(mutableListOf(), activity!!.applicationContext)

        val myList = rootView.findViewById(R.id.myList) as RecyclerView
        myList.layoutManager = LinearLayoutManager(activity)
        myList.adapter = adapter

        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val viewModel = MyListViewModel(activity!!.application)
        CoroutineScope(Dispatchers.Default).launch {
            viewModel.getMyTasksFromFDB()
        }

        val isConnected = viewModel.isConnected

        val pref = viewModel.pref
        val prefValue = pref.getBoolean("pref", false)

        if (isConnected) {
            viewModel.myTasks.observe(viewLifecycleOwner, Observer {
                adapter.setMyList(it)
            })
        } else {
            viewModel.repository.allTasks.observe(viewLifecycleOwner, Observer {
                adapter.setMyList(it)
            })
        }

        adapter.onMyItemHold = { task ->
            if (isConnected) {
                val dialogBuilder = AlertDialog.Builder(activity)
                dialogBuilder
                    .setMessage(getString(R.string.delete_question))
                    .setCancelable(true)
                    .setPositiveButton("Yes") { _, _ ->
                        viewModel.deleteMyTask(task.id)
                        Toast.makeText(
                            activity!!.applicationContext,
                            getString(
                                if (prefValue)
                                    R.string.deleted_my_task_meme
                                else
                                    R.string.item_deleted
                            ),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    .setNegativeButton("No") { dialog, _ ->
                        dialog.cancel()
                    }
                val alert = dialogBuilder.create()
                alert.setTitle(getString(R.string.delete))
                alert.show()
            } else {
                Toast.makeText(
                    activity!!.applicationContext,
                    getString(
                        if (prefValue)
                            R.string.no_network_meme
                        else
                            R.string.no_network
                    ),
                    Toast.LENGTH_SHORT
                ).show()

            }
        }
        adapter.onMyItemClick = { task ->
            if (isConnected) {
                viewModel.toMainList(task.id, task.name, task.orphanage)
                adapter.notifyItemRemoved(0)
                Toast.makeText(
                    activity!!.applicationContext,
                    getString(
                        if (prefValue)
                            R.string.to_main_list_meme
                        else
                            R.string.to_main_list
                    ),
                    Toast.LENGTH_SHORT
                ).show()

            } else {
                Toast.makeText(
                    activity!!.applicationContext,
                    getString(
                        if (prefValue)
                            R.string.no_network_meme
                        else
                            R.string.no_network
                    ),
                    Toast.LENGTH_SHORT
                ).show()

            }
        }
    }
}
