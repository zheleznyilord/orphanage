package com.geekhub.orphanage.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
@Entity(tableName = "tasks")
data class Task(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "task_id")
    val id: String = "",
    @ColumnInfo(name = "name")
    val name: String? = "",
    @ColumnInfo(name = "orphanage")
    val orphanage: String? = ""
){
    @Exclude
    fun toMap(): Map<String, Any?>{
        return mapOf(
            "id" to id,
            "name" to name,
            "orphanage" to orphanage
        )
    }
}