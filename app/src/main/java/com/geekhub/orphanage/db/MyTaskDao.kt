package com.geekhub.orphanage.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.geekhub.orphanage.db.entities.MyTask

@Dao
interface MyTaskDao {
    @Query("SELECT * from my_tasks")
    fun getTasksFromDao(): LiveData<MutableList<MyTask>>

    @Query("SELECT * FROM my_tasks WHERE id =:id")
    fun getMyTask(id: String): MyTask

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(task: MyTask)

    @Query("DELETE FROM my_tasks")
    suspend fun deleteAll()

    @Delete
    suspend fun deleteTask(task: MyTask)
}