package com.geekhub.orphanage.viewModel

import com.geekhub.orphanage.db.entities.Task
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.nhaarman.mockitokotlin2.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.powermock.api.mockito.PowerMockito
import org.powermock.api.mockito.PowerMockito.`when`
import org.powermock.api.mockito.PowerMockito.mockStatic
import org.powermock.core.classloader.annotations.PowerMockIgnore
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner
import org.powermock.modules.junit4.PowerMockRunnerDelegate
import org.powermock.modules.junit4.rule.PowerMockRule


@RunWith(MockitoJUnitRunner.Silent::class)
class AddTaskViewModelTest { //this shit doesn't work but i tried

    @Mock
    lateinit var mockedDatabase: FirebaseDatabase

    @Mock
    lateinit var databaseReference: DatabaseReference

    @Mock
    lateinit var viewModel: AddTaskViewModel

    private val push: DatabaseReference = mock()
    private val child1 = Mockito.mock(DatabaseReference::class.java)
    private val child2 : DatabaseReference = mock()

    @Before
    fun before() {
        databaseReference = Mockito.mock(DatabaseReference::class.java)
        mockedDatabase = Mockito.mock(FirebaseDatabase::class.java)
        doReturn(databaseReference).whenever(mockedDatabase).reference

        viewModel = AddTaskViewModel(mockedDatabase)
    }

    @Test
    fun writeNewTaskTest() {
        val id = "1"
        doReturn(push).whenever(databaseReference).push()
        doReturn(id).whenever(push).key
        doReturn(child1).whenever(databaseReference).child("tasks")
        doReturn(child2).whenever(child1).child(id)
        val name = "name"
        val orphanage = "orphanage"
        val task = Task(id, name, orphanage)

        viewModel.writeNewTask(name, orphanage)

        verify(databaseReference)
            .child("tasks")
            .child(id)
            .setValue(task)
    }
}